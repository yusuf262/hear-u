//
//  PrimaryButton.swift
//  MealMonkey
//
//  Created by MacBook Pro on 30/03/23.
//

import UIKit

@IBDesignable
class PrimaryButton: UIButton {
    @IBInspectable var cornerRadius: CGFloat = 25 {
        didSet { update() }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        setup()
    }
    
    override func prepareForInterfaceBuilder() {
        super.prepareForInterfaceBuilder()
        setup()
    }
    
    convenience init(){
        self.init(type: .system)
        setup()
        
    }
    
    func setup(){
        backgroundColor = UIColor(named: "Main2", in: Bundle(for: PrimaryButton.self), compatibleWith: nil)
        titleLabel?.font = UIFont.systemFont(ofSize: 14, weight: .semibold)
        tintColor = .black
        
        update()
    }
    func update() {
        layer.cornerRadius = self.cornerRadius
        layer.masksToBounds = true
    }
}
