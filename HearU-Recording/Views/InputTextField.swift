//
//  InputTextField.swift
//  MealMonkey
//
//  Created by MacBook Pro on 30/03/23.
//

import UIKit

@IBDesignable
class InputTextField: UITextField {
    
    @IBInspectable var cornerRadius: CGFloat = 25 {
        didSet { updaet() }
    }
    
    @IBInspectable var padding: CGFloat = 16 {
        didSet { layoutIfNeeded() }
    }
    override func awakeFromNib() {
        super.awakeFromNib()
        setup()
    }
    
    override func prepareForInterfaceBuilder() {
        super.prepareForInterfaceBuilder()
        setup()
    }
    
    convenience init(){
        self.init(frame: .zero)
        setup()
    }
    
    override func textRect(forBounds bounds: CGRect) -> CGRect {
        return bounds.inset(by: UIEdgeInsets(top: 0, left: padding, bottom: 0, right: padding))
    }
    
    override func placeholderRect(forBounds bounds: CGRect) -> CGRect {
        return bounds.inset(by: UIEdgeInsets(top: 0, left: padding, bottom: 0, right: padding))
    }
    
    override func editingRect(forBounds bounds: CGRect) -> CGRect {
        return bounds.inset(by: UIEdgeInsets(top: 0, left: padding, bottom: 0, right: padding))
    }
    
    func setup() {
        textColor = UIColor(named: "Main", in: Bundle(for: InputTextField.self), compatibleWith: nil)
        font = UIFont.systemFont(ofSize: 14, weight: .regular)
        updaet()
    }
    
    func updaet() {
        layer.cornerRadius = self.cornerRadius
        layer.masksToBounds = true
    }
}
