//
//  AuthViewController.swift
//  HearU-Recording
//
//  Created by MacBook Pro on 04/04/23.
//

import UIKit
import UIKit
import AVFoundation
import CoreData
import AVKit
import AVFAudio

class ChipmunkViewController: UIViewController, AVAudioRecorderDelegate, AVAudioPlayerDelegate{

    var soundsNoteID: String!        // populated from incoming seque
    var soundsNoteTitle: String!     // populated from incoming seque
    var soundURL: String!            // store in CoreData
    var recordingSession: AVAudioSession!
    var audioRecorder: AVAudioRecorder!
    var audioPlayer:AVAudioPlayer!
    
    var audioEngine = AVAudioEngine()
    let audioPlayerNode = AVAudioPlayerNode()
    let timePitchUnit = AVAudioUnitTimePitch()
    
    var currentDate = Date()
    let dateFormatter = DateFormatter()
    
    @IBOutlet weak var statusLabel: UILabel!
    @IBOutlet weak var recordButtonOutlet: UIButton!
    @IBOutlet weak var stopButtonOutlet: UIButton!
    @IBOutlet weak var playButtonOutlet: UIButton!
    override func viewDidLoad() {
        super.viewDidLoad()
        statusLabel.text = "Ready to record"
        // Do any additional setup after loading the view.
        self.setupView()
        
    }
    
    func setupView() {
        recordingSession = AVAudioSession.sharedInstance()
        
        do {
            try recordingSession.setCategory(.playAndRecord, mode: .default)
            try recordingSession.setActive(true, options: .notifyOthersOnDeactivation)
            recordingSession.requestRecordPermission() { [unowned self] allowed in
                DispatchQueue.main.async {
                    if allowed {
                        self.loadRecordingUI()
                    } else {
                        // failed to record
                    }
                }
            }
        } catch {
            // failed to record
            print(error.localizedDescription)
        }
    }
    func loadRecordingUI() {
        recordButtonOutlet.isEnabled = true
        stopButtonOutlet.isEnabled = true
        playButtonOutlet.isEnabled = true
    }
    
    
    @IBAction func recordButtonTapped(_ sender: Any) {
        if audioRecorder == nil{
            try? startRecording()
        } else {
            finishRecording()
        }
    }
    
    func startRecording() throws {
        statusLabel.text = "Recording...."
        //        let audioFilename = getFileURL()
        let settings = [AVFormatIDKey: Int(kAudioFormatMPEG4AAC),
                      AVSampleRateKey: 44100,
                AVNumberOfChannelsKey: 1,
             AVEncoderAudioQualityKey: AVAudioQuality.high.rawValue]
        
        let audioURL = getFileURL()
        print(audioURL)
        do {
            try recordingSession.setCategory(.playAndRecord, mode: .default)
            try recordingSession.setActive(true, options: .notifyOthersOnDeactivation)
            audioRecorder = try AVAudioRecorder(url: audioURL, settings: settings)
            audioRecorder?.prepareToRecord()
            audioRecorder.delegate = self
            audioRecorder?.record()
        } catch {
            print("Error starting recording")
            finishRecording()
        }
    }
    
    func finishRecording() {
        statusLabel.text = "Recording Completed"
        audioRecorder.stop()
        audioRecorder = nil
        recordButtonOutlet.isEnabled = true
        playButtonOutlet.isEnabled = true
        stopButtonOutlet.isEnabled = true
    }
    @IBAction func stopButtonTapped(_ sender: Any) {
        playButtonOutlet.isEnabled = true
        stopButtonOutlet.isEnabled = true
        recordButtonOutlet.isEnabled = true
        statusLabel.text = "Done"
        audioPlayerNode.stop()
        audioEngine.stop()
        audioEngine.reset()
        currentDate = Date()
        
    }
    
    
    @IBAction func playButtonTapped(_ sender: Any) {
        playButtonOutlet.isEnabled = true
        recordButtonOutlet.isEnabled = true
        stopButtonOutlet.isEnabled = true
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
            try? self.pitchAudioFile(pitch: 1200)
        }
    }
    
    func getDocumentsDirectory() -> URL {
        let paths = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)
        return paths[0]
    }
    func getFileURL() -> URL {
        dateFormatter.dateFormat = "YYYY-MM-dd HH:mm:ss"
        dateFormatter.timeZone = TimeZone.current
        let dateNow = dateFormatter.string(from: currentDate)
        let audioURL = getDocumentsDirectory().appendingPathComponent("\(dateNow).m4a")
        return audioURL
    }
    func getFileChangeURL() -> URL {
        dateFormatter.dateFormat = "YYYY-MM-dd HH:mm:ss"
        dateFormatter.timeZone = TimeZone.current
        let dateNow = dateFormatter.string(from: currentDate)
        let audioURL = getDocumentsDirectory().appendingPathComponent("\(dateNow)_changed.m4a")
        return audioURL
    }
    
    func audioRecorderDidFinishRecording(_ recorder: AVAudioRecorder, successfully flag: Bool) {
        if !flag {
            finishRecording()
        }
    }
    
    func audioRecorderEncodeErrorDidOccur(_ recorder: AVAudioRecorder, error: Error?) {
        print("Error while recording audio \(error!.localizedDescription)")
    }
    
    func audioPlayerDidFinishPlaying(_ player: AVAudioPlayer, successfully flag: Bool) {
        recordButtonOutlet.isEnabled = true
        playButtonOutlet.isEnabled = true
    }
    
    func audioPlayerDecodeErrorDidOccur(_ player: AVAudioPlayer, error: Error?) {
        print("Error while playing audio \(error!.localizedDescription)")
    }
    

    func pitchAudioFile(pitch: Float) throws{
        statusLabel.text = "Playing Audio"
        // Create audio engine and player node
        let inputURL = getFileURL()
//        let outputURL = getFileChangeURL()
        timePitchUnit.pitch = pitch
        
        // Attach player node and time pitch unit to audio engine
        audioEngine.attach(audioPlayerNode)
        audioEngine.attach(timePitchUnit)
        
        // Connect player node to time pitch unit
        audioEngine.connect(audioPlayerNode, to: timePitchUnit, format: nil)
        
        // Connect time pitch unit to main mixer node
        let mixer = audioEngine.mainMixerNode
        audioEngine.connect(timePitchUnit, to: mixer, format: nil)
        
        // Schedule audio file to be played by player node
        let audioFile = try AVAudioFile(forReading: inputURL)
        
        audioPlayerNode.scheduleFile(audioFile, at: nil)
        
        
        // Start audio engine
        audioEngine.prepare()
        try audioEngine.start()
        
        // Wait for playback to finish
        audioPlayerNode.volume = 10.0
        audioPlayerNode.play()
        
        while audioPlayerNode.isPlaying {
            RunLoop.current.run(until: Date(timeIntervalSinceNow: 0))
        }
        
        // Save modified audio file to disk
//        let outputFormat = AVAudioFormat(commonFormat: .pcmFormatFloat32, sampleRate: audioFile.fileFormat.sampleRate, channels: audioFile.fileFormat.channelCount, interleaved: false)!
//
//        let outputFile = try AVAudioFile(forWriting: outputURL, settings: audioFile.fileFormat.settings)
//        let buffer = AVAudioPCMBuffer(pcmFormat: audioFile.processingFormat, frameCapacity: AVAudioFrameCount(audioFile.length))!
//        try! audioFile.read(into: buffer)
//        try! outputFile.write(from: buffer)
//
    }
        
}
